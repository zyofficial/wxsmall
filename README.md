### 开发资料 
* 小程序接口文档：https://developers.weixin.qq.com/miniprogram/dev/index.html
* phpDemo：https://github.com/tencentyun/wafer2-quickstart-php


### 小程序开发流程
```
1、注册企业小程序 
2、下载开发者工具 
3、添加开发者,进行代码上传（获取APPID） 
4、配置服务器域名（注册地址对网络协议要求） 
5、应用开发（环境搭建，代码开发） 
6、提交开发版本 
7、管理员根据上传的开发版本，选择版本提交审核 
  7.1 配置功能页面（填写重要业务页面的类目与标签，重要业务页面组数不多于5组） 
  7.2 测试账号（当小程序需要开发者提供测试帐号才能完成审核体验时，小程序在首次提交审核时将被打回，再次提交审核时将开放提供测试帐号的入口，该入口将由开发者提供帐号给微信审核人员审核微信小程序时登录使用。）
  7.3 完成提交（提交审核完成后，开发管理页中审核版本模块展示审核进度。）
8、代码审核通过，需要开发者手动点击发布，小程序才会发布到线上提供服务。 



Ps: 

小程序审核拒绝情形：<https://developers.weixin.qq.com/miniprogram/product/reject.html?t=18122617> 
小程序流程文档：   <https://developers.weixin.qq.com/miniprogram/introduction/#> 
申请微信支付：     <https://developers.weixin.qq.com/miniprogram/introduction/#>小程序申请微信支付 
绑定开放平台账号： <https://developers.weixin.qq.com/miniprogram/introduction/#>小程序绑定微信开放平台帐号 
公众号关联小程序： <https://developers.weixin.qq.com/miniprogram/introduction/#>公众号关联小程序
```

